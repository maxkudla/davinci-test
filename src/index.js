import React from 'react';
import ReactDOM from 'react-dom';
import App from './js/app';

import Perf from 'react-addons-perf'
window.Perf = Perf;

import './index.css';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
