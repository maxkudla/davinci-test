import React, { Component } from 'react';

import './app.css';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';

import RoomOptionsDialog from './containers/RoomOptionsDialog';

injectTapEventPlugin();

class App extends Component {
    render() {
        return (
            <MuiThemeProvider>
                <div className="App">
                    <div className="App-header">
                        <h2>Welcome to React</h2>
                    </div>
                    <RoomOptionsDialog />
                </div>
            </MuiThemeProvider>
        );
    }
}

export default App;
