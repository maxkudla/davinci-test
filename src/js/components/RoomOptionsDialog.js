import React, {PropTypes} from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/navigation/close';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';

const title_styles = {
    wrapper: {
        padding: "20px 24px 14px",
        backgroundColor: "#eee",
        margin: "-24px -24px 20px -24px"
    },
    body: {
        verticalAlign: "top",
        display: "inline-block"
    },
    actions: {
        float: "right",
        verticalAlign: "top"
    },
    action_close: {
        padding: 0,
        height: 'auto',
        width: 'auto',
    },
    clearfix: {
        clear: 'both'
    }
};
const RoomOptionsDialogTitle = React.createClass({
    propTypes: {
        onClose: PropTypes.func,
    },

    mixins: [PureRenderMixin],

    render: function () {
        return (
            <div style={title_styles.wrapper}>
                <div style={title_styles.body}>
                    <span>Структура номера</span>
                </div>

                <div style={title_styles.actions}>
                    <IconButton style={title_styles.action_close} onTouchTap={this.props.onClose}>
                        <CloseIcon />
                    </IconButton>
                </div>
                <div style={title_styles.clearfix} />
            </div>
        );
    }
});

const option_style = {
    select: {
        display: "inline-block",
        verticalAlign: "top",
        marginRight: 15,
        float: "left",
        width: 150
    },
    text: {
        display: "inline-block",
        verticalAlign: "top",
        marginRight: 15,
        float: "left",
        width: 150
    },
    actions: {
        float: 'right'
    }
};
const RoomOptionRow = React.createClass({
    propTypes: {
        option: PropTypes.object,

        onRemove: PropTypes.func,
        onChange: PropTypes.func,
    },

    mixins: [PureRenderMixin],

    handleSizeChange (event, index, size){
        this.props.onChange(this.props.option,{
            size
        })
    },

    handleCountChange ({target: {value: count }}){
        this.props.onChange(this.props.option,{
            count: + count
        });
    },

    handleRemove(){
        this.props.onRemove(this.props.option)
    },

    render() {
        let {size, count} =  this.props.option;
        return (
            <div>
                <SelectField style={option_style.select}
                             value={size || 1}
                             onChange={this.handleSizeChange}>
                    <MenuItem value={1} primaryText="Standard"/>
                    <MenuItem value={2} primaryText="Twin"/>
                    <MenuItem value={3} primaryText="Tripple"/>
                    <MenuItem value={4} primaryText="Quadro"/>
                </SelectField>

                <TextField style={option_style.text} hintText="Count"
                           value={count || ''}
                           onChange={this.handleCountChange}/>

                <div style={option_style.actions}>
                    <FloatingActionButton mini={true} secondary={true}
                                          style={{margin: 10}}
                                          iconStyle={{    height: 30,width: 30}}
                                          onTouchTap={this.handleRemove}>
                        <CloseIcon        style={{    height: 20,width: 20, margin: 5}} />
                    </FloatingActionButton>
                </div>
            </div>
        );
    }
});

const options_dialog_style = {
    actions: {
        marginTop: 20,
        textAlign: "right"
    }
};
const RoomOptionsDialogAddButton = React.createClass({
    propTypes: {
        onClick: PropTypes.func,
    },

    mixins: [PureRenderMixin],

    render() {
        return (
            <div>
                <FlatButton label="ADD" primary={true} onTouchTap={this.props.onClick} />
            </div>
        );
    }
});
const RoomOptionsDialogActions = React.createClass({
    propTypes: {
        onSave: PropTypes.func,
        onCancel: PropTypes.func,
    },

    mixins: [PureRenderMixin],

    render() {
        return (
            <div style={options_dialog_style.actions}>
                <RaisedButton label="SAVE" primary={true} onTouchTap={this.props.onSave} />
                <FlatButton label="CANCEL" primary={true} style={{marginLeft:5}}
                            onTouchTap={this.props.onCancel} />
            </div>
        );
    }
});

class RoomOption {
    constructor(){
        this.id = + new Date();
        this.size = 1;
        this.count = 1;
    }
}
const RoomOptionsDialog = React.createClass({
    propTypes: {
        options: PropTypes.array,

        onRequestClose: PropTypes.func,
        onRequestResolve: PropTypes.func
    },

    mixins: [PureRenderMixin],

    getInitialState() {
        return {
            options: this.props.options.concat()
        };
    },

    getDefaultProps() {
        return {
            options: [new RoomOption()],
            open: false,
            modal: false
        };
    },

    handleRemoveOption(option){
        let option_index = this.state.options.indexOf(option);
        this.state.options.splice(option_index,1);
        this.forceUpdate();
    },

    handleAddOption(){
        this.state.options.push(new RoomOption());
        this.forceUpdate();
    },

    handleChangeOption(option,data){
        let option_index = this.state.options.indexOf(option);
        this.state.options.splice(option_index,1,Object.assign({},option,data));
        this.forceUpdate();
    },

    handleSaveOptions(){
        this.props.onRequestResolve(this.state.options);
    },

    render () {
        return (
            <div>
                <RoomOptionsDialogTitle onClose={this.props.onRequestClose}/>
                {
                    this.state.options.map((option) => (
                        <RoomOptionRow ref={option.id} key={option.id} option={option}
                                       onChange={this.handleChangeOption}
                                       onRemove={this.handleRemoveOption} />
                    ))
                }
                <RoomOptionsDialogAddButton onClick={this.handleAddOption} />
                <RoomOptionsDialogActions onSave={this.handleSaveOptions}
                                          onCancel={this.props.onRequestClose}/>
            </div>
        );
    }
});

const dialog_style = {
    width: 450
};

export default React.createClass({
    propTypes: {
        open: PropTypes.bool,
        onRequestClose: PropTypes.func,
    },

    render() {
        return (
            <Dialog open={this.props.open} contentStyle={dialog_style}
                    onRequestClose={this.props.onRequestClose}>

                <RoomOptionsDialog options={this.props.options}
                                   onRequestResolve={this.props.onRequestResolve}
                                   onRequestClose={this.props.onRequestClose} />

            </Dialog>
        );
    }
});
