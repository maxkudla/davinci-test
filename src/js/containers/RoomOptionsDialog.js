import React  from 'react';

import RaisedButton from 'material-ui/RaisedButton';

import RoomOptionsDialog  from '../components/RoomOptionsDialog';

export default React.createClass({
    getInitialState() {
        return {
            open: false
        };
    },

    handleClose(){
        this.setState({
            open: false
        })
    },

    handleShow(){
        this.setState({
            open: true
        })
    },

    handleOptionsSave(options){
        console.log(options);
        this.setState({options},
            this.handleClose
        );
    },

    render() {
        return (
            <div>
                <RaisedButton label="Room options" onTouchTap={this.handleShow}/>
                <RoomOptionsDialog open={this.state.open}
                                   options={this.state.options}
                                   onRequestResolve={this.handleOptionsSave}
                                   onRequestClose={this.handleClose}
                    />
            </div>
        );
    }
});