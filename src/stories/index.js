import React from 'react';
import { storiesOf, action, linkTo } from '@kadira/storybook';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';

import RoomOptionsDialog from '../js/components/RoomOptionsDialog';

injectTapEventPlugin();

storiesOf('RoomOptionsDialog', module)
    .add('without initial data', () => (
        <MuiThemeProvider>
            <RoomOptionsDialog open={true}
                               onRequestResolve={action('handleSave')}
                               onRequestClose={action('handleClose')} />
        </MuiThemeProvider>

    ))
    .add('with initial data', () => (
        <MuiThemeProvider>
            <RoomOptionsDialog open={true}
                               options={[{"id":1482580855669,"size":1,"count":1},{"id":1482580862256,"size":2,"count":12}]}
                               onRequestResolve={action('handleSave')}
                               onRequestClose={action('handleClose')} />
        </MuiThemeProvider>

    ));
